<html lang="en">
<head>
  <meta http-equiv=Content-Type content="text/html; charset=utf-8">
  <title>บันทึกข้อมูล</title>
  <link rel="stylesheet" type="text/css" href="./css/util.css">
<link rel="stylesheet" type="text/css" href="./css/main.css">
</head>
  <script type="text/javascript" src="./js/sweetalert.min.js"></script>


<body class="BodyCus">
  <script>


  function FailedToSaveTheRecord() {

    swal({
      title: 'ผิดพลาด',
      text: 'การลงทะเบียนผิดพลาดกรุณาลองใหม่',
      timer: 3000,
      showConfirmButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
      type: 'error',
    }).then((result) => {
      if (
        result.dismiss === swal.DismissReason.timer
      ) {
        // ทำหลังจากเวลาหมด
        window.location = "./RegisForm.php";
      }
    })
};

  function RecaptchaNeeded() {
  swal({
    title: 'ผิดพลาด',
    text: 'กรุณายืนยันความเป็นมนุษย์ก่อน',
    timer: 3000,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    type: 'error',
  }).then((result) => {
    if (
      result.dismiss === swal.DismissReason.timer
    ) {
      // ทำหลังจากเวลาหมด
      window.location = "./RegisForm.php";
    }
  })
};

function FillAllField() {
swal({
  title: 'ผิดพลาด',
  text: 'กรุณากรอกข้อมูลให้ครบถ้วน',
  timer: 3000,
  showConfirmButton: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
  type: 'error',
}).then((result) => {
  if (
    result.dismiss === swal.DismissReason.timer
  ) {
    // ทำหลังจากเวลาหมด
    window.location = "./RegisForm.php";
  }
})
};


</script>
</body>

      </html>
<?php

session_start();

          require('ConnectToService.php');

        $_SESSION["FirstName_Var"]=  $FirstName_Var     =    mysqli_real_escape_string($conn, $_POST['FirstName']);
        $_SESSION["LastName_Var"]=  $LastName_Var      =    mysqli_real_escape_string($conn, $_POST['LastName']);
        $_SESSION["Identify_Var"]=  $Identify_Var      =    mysqli_real_escape_string($conn, $_POST['Identify']);
        $_SESSION["Email_Var"]=  $Email_Var         =    mysqli_real_escape_string($conn, $_POST['Email']);
        $_SESSION["BirthdayDay"]=  $BirthdayDay       =    mysqli_real_escape_string($conn, $_POST['Birthday']);
        $_SESSION["PhoneNumber_Var"]=  $PhoneNumber_Var   =    mysqli_real_escape_string($conn, $_POST['PhoneNumber']);
        $_SESSION["Course_Var"]=  $Course_Var        =    mysqli_real_escape_string($conn, $_POST['Course']);
        $_SESSION["StudentYear_Var"]=  $StudentYear_Var   =    mysqli_real_escape_string($conn, $_POST['StudentYear']);
        $_SESSION["ClassNumber_Var"]=  $ClassNumber_Var   =    mysqli_real_escape_string($conn, $_POST['ClassNumber']);
        $_SESSION["Department_Var"]=  $Department_Var    =    mysqli_real_escape_string($conn, $_POST['Department']);
        $_SESSION["JoinYear"]=  $JoinYear          =    mysqli_real_escape_string($conn, $_POST['JoinYear']);




          $Identify_Number   = strlen($Identify_Var);
          for ($i=0; $Identify_Number > 3 ; $i++) {
              $Identify_Number--;
          }

          $ClassNumber_Var = "0" . $ClassNumber_Var;


          $BirthdayDay = explode('/', $BirthdayDay);
          $Date_Var          =    $BirthdayDay[0];
          $Month_Var         =    $BirthdayDay[1];
          $Years_Var         =    $BirthdayDay[2];
          $Years_Var         =    $Years_Var+543;

          $_SESSION["Date_Var"] = $Date_Var;
          $_SESSION["Month_Var"] = $Month_Var;
          $_SESSION["Years_Var"] = $Years_Var;


          $GenarateIdentify = substr($Identify_Var, -$Identify_Number);

          $JoinYearGenarator = substr($JoinYear, -2);
          $genarateIdentify = substr($Identify_Var, -2);




          $UsernameGenarate = $JoinYearGenarator . $Course_Var . $Department_Var . $ClassNumber_Var . $GenarateIdentify;
          $PasswordGenarate = $Date_Var . $Month_Var . $Years_Var;

          $_SESSION["UsernameGenarate"] = $UsernameGenarate;
          $_SESSION["PasswordGenarate"] = $PasswordGenarate;

          /*
          echo " ชื่อผู้ใช้ของคุณคือ : ";
          echo $UsernameGenarate;
          echo " รหัสผ่านของคุณคือ : ";
          echo $PasswordGenarate;
          */
          $sql = "INSERT INTO
  `UserInformation` (
    `name`,
    `lastname`,
    `ID_Passport`,
    `Email`,
    `BirthDate`,
    `BirthMonth`,
    `BirthYears`,
    `Course`,
    `StudentYear`,
    `ClassNumber`,
    `Department`,
    `PhoneNumber`,
    `GeneratedUser`,
    `GeneratedPass`,
    `JoinYear`
  )
VALUES
  (
    '$FirstName_Var',
    '$LastName_Var',
    '$Identify_Var',
    '$Email_Var',
    '$Date_Var',
    '$Month_Var',
    '$Years_Var',
    '$Course_Var',
    '$StudentYear_Var',
    '$ClassNumber_Var',
    '$Department_Var',
    '$PhoneNumber_Var',
    '$UsernameGenarate',
    '$PasswordGenarate',
    '$JoinYear'
  )";

// ==============================================================================================================================


$DepartmentSql = "SELECT * FROM `DepartmentManager` ";
$resultDepartment = $conn->query($DepartmentSql);

$CourseArray = array("ปวช", "ปวส");
$DepartmentArray = array();

if ($resultDepartment->num_rows > 0) {
    while ($row = $resultDepartment->fetch_assoc()) {
        array_push($DepartmentArray, array($row["Department_ID"],$row["Department_Name"]));
    }
}


  $DepartmentName = $DepartmentArray[$Department_Var-1][1];

$_SESSION["DepartmentName"] = $DepartmentName;



sleep(5);
header("location: ./PDFGenarator/Genarate.php");



          if ($conn->query($sql) === true) {
              echo '<script type="text/javascript">',
"swal({
  title: 'ลงทะเบียนสำเร็จ',
  text: 'กรุณานำเอกสารยืนยันยื่นกับผู้ดูแล',
  confirmButtonText: 'เข้าใจแล้ว',
  showConfirmButton: true,
  allowOutsideClick: false,
  type: 'success',
}).then((result) => {
  if (result.value) {
    window.location = './RegisForm.php';
  }
})",
'</script>';
          } else {
              echo '<script type="text/javascript">',
   'FailedToSaveTheRecord();',
   '</script>';
          }
?>

<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <!--===============================================================================================-->
</head>
