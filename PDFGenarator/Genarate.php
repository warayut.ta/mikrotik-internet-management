<?php
session_start();
/***************************
  Sample using a PHP array
****************************/



    $FirstName_Var =   $_SESSION["FirstName_Var"];
    $LastName_Var  =  $_SESSION["LastName_Var"];
    $Identify_Var =   $_SESSION["Identify_Var"];
    $Email_Var  =  $_SESSION["Email_Var"];
    $BirthdayDay =   $_SESSION["BirthdayDay"];
    $PhoneNumber_Var  =  $_SESSION["PhoneNumber_Var"];
    $Course_Var  =  $_SESSION["Course_Var"];
    $StudentYear_Var =   $_SESSION["StudentYear_Var"];
    $ClassNumber_Var  =  $_SESSION["ClassNumber_Var"];
    $Department_Var   = $_SESSION["Department_Var"];
    $JoinYear    =$_SESSION["JoinYear"];


$UsernameGenarate  =    $_SESSION["UsernameGenarate"];
$PasswordGenarate  =    $_SESSION["PasswordGenarate"];
$DepartmentName    =    $_SESSION["DepartmentName"];
$BirthdayDay = explode('/', $BirthdayDay);
$Date_Var          =    $_SESSION["Date_Var"];
$Month_Var         =    $_SESSION["Month_Var"];
$Years_Var         =    $_SESSION["Years_Var"];



require('fpdm.php');

$Identify_Var_Split = str_split($Identify_Var, 1);
$NewArray = array();
foreach ($Identify_Var_Split as &$valueID) {
    array_push($NewArray, $valueID);
}

$SumAllDate = $Date_Var . $Month_Var . $Years_Var;

$BirthDay_Split = str_split($SumAllDate, 1);
$BirthDayArray = array();

foreach ($BirthDay_Split as &$valueBD) {
    array_push($BirthDayArray, $valueBD);
}

if($Course_Var == 1){
  $fields = array(
      'Course_1'   			  			=> 'X',
      'Course_2'    						=> ' ',
      'FirstnameTop' 			    	=> $FirstName_Var,
      'LastnameTop'  		 			  => $LastName_Var,
      'Department'  		 			  => $DepartmentName,
      'FirstnameBot' 	   			  => $FirstName_Var,
      'LastnameBot' 	   			  => $LastName_Var,
      'Email'   				 			  => $Email_Var,
      '1_ID'    								=> $NewArray[0],
      '2_ID'    								=> $NewArray[1],
      '3_ID'   					 			  => $NewArray[2],
      '4_ID'   					 			  => $NewArray[3],
      '5_ID'   					  			=> $NewArray[4],
      '6_ID'  					 			  => $NewArray[5],
      '7_ID'  					 		 	  => $NewArray[6],
      '8_ID'  					 			  => $NewArray[7],
      '9_ID'  					  			=> $NewArray[8],
      '10_ID'    								=> $NewArray[9],
      '11_ID'    								=> $NewArray[10],
      '12_ID'  					  			=> $NewArray[11],
      '13_ID'    		 						=> $NewArray[12],
      'StudentYears'   	  			=> $StudentYear_Var,
      '1_BD'  					 			  => $BirthDayArray[0],
      '2_BD'   					  			=> $BirthDayArray[1],
      '3_BD'    								=> $BirthDayArray[2],
      '4_BD'    								=> $BirthDayArray[3],
      '5_BD'    								=> $BirthDayArray[4],
      '6_BD'    								=> $BirthDayArray[5],
      '7_BD'    								=> $BirthDayArray[6],
      '8_BD'    								=> $BirthDayArray[7],
      'StudentRoom'    					=> $ClassNumber_Var,
      'PhoneNumber'    					=> $PhoneNumber_Var,
      'GenaratedUsernameTop'    => $UsernameGenarate,
      'GenaratedPasswordTop'    => $PasswordGenarate,
      'GenaratedUsername'    		=> $UsernameGenarate,
      'GenaratedPassword'    		=> $PasswordGenarate
    );
}else{
$fields = array(
    'Course_1'   			  			=> ' ',
    'Course_2'    						=> 'X',
    'FirstnameTop' 			    	=> $FirstName_Var,
    'LastnameTop'  		 			  => $LastName_Var,
    'Department'  		 			  => $DepartmentName,
    'FirstnameBot' 	   			  => $FirstName_Var,
    'LastnameBot' 	   			  => $LastName_Var,
    'Email'   				 			  => $Email_Var,
    '1_ID'    								=> $NewArray[0],
    '2_ID'    								=> $NewArray[1],
    '3_ID'   					 			  => $NewArray[2],
    '4_ID'   					 			  => $NewArray[3],
    '5_ID'   					  			=> $NewArray[4],
    '6_ID'  					 			  => $NewArray[5],
    '7_ID'  					 		 	  => $NewArray[6],
    '8_ID'  					 			  => $NewArray[7],
    '9_ID'  					  			=> $NewArray[8],
    '10_ID'    								=> $NewArray[9],
    '11_ID'    								=> $NewArray[10],
    '12_ID'  					  			=> $NewArray[11],
    '13_ID'    		 						=> $NewArray[12],
    'StudentYears'   	  			=> $StudentYear_Var,
    '1_BD'  					 			  => $BirthDayArray[0],
    '2_BD'   					  			=> $BirthDayArray[1],
    '3_BD'    								=> $BirthDayArray[2],
    '4_BD'    								=> $BirthDayArray[3],
    '5_BD'    								=> $BirthDayArray[4],
    '6_BD'    								=> $BirthDayArray[5],
    '7_BD'    								=> $BirthDayArray[6],
    '8_BD'    								=> $BirthDayArray[7],
    'StudentRoom'    					=> $ClassNumber_Var,
    'PhoneNumber'    					=> $PhoneNumber_Var,
    'GenaratedUsernameTop'    => $UsernameGenarate,
    'GenaratedPasswordTop'    => $PasswordGenarate,
    'GenaratedUsername'    		=> $UsernameGenarate,
    'GenaratedPassword'    		=> $PasswordGenarate
  );
}

$pdf = new FPDM('newGen.pdf');
$pdf->Load($fields, true); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
$pdf->Merge();
$pdf->Output('D', "เอกสารลงทะเบียน.pdf");
