<?php
session_start();

  if (isset($_SESSION['user_id'])) {
      header("location:./Adminmanager.php");
  }

  ?>


<html lang="en">

<head>
  <title>ระบบอินเตอร์เน็ตวิทยาลัยการอาชีพปัว</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/utilLogin.css">
  <link rel="stylesheet" type="text/css" href="css/mainLogin.css">
  <!--===============================================================================================-->
  <script type="text/javascript" src="js/sweetalert.min.js"></script>
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
  <script>
  function LoginError() {
    Swal.fire({
      title: 'ไม่พบข้อมูล',
      text: 'กรุณาลองใหม่อีกครั้ง',
      showConfirmButton: false,
      timer: 3000,
      type: 'error',
    })
  }
  function EmptyInput() {
    Swal.fire({
      title: 'กรุณากรอกข้อมูล',
      text: 'กรุณากรอกข้อมูลให้ครบถ้วน',
      showConfirmButton: false,
      timer: 3000,
      type: 'error',
    })
  }

  function LoginSucces(AdminName) {
    Swal.fire({
      title: 'เข้าสู่ระบบสำเร็จ',
      text: 'ยินดีต้อนรับคุณ ' + AdminName + ' เข้าสู่ระบบ',
      showConfirmButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
      timer: 3000,
      type: 'success',
    }).then((result) => {
  if (
    result.dismiss === swal.DismissReason.timer
  ) {
    // ทำหลังจากเวลาหมด
      window.location = "./Adminmanager.php";
  }
})
  }


  function SendAjaxAction(){
    $.ajax({
     type: "POST",
     url: "Auth.php",
     dataType:"json",
     data: { Admin_Username : $("#Admin_Username").val() ,Admin_Password : $("#Admin_Password").val()}
   }).done(function( data ) {
     if(data[0] === "LoginSucces"){
      LoginSucces(data[1]);
    }else{
      LoginError();
    }
   });
  }
  </script>

  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic">
          <img src="images/AdminPage.png" alt="IMG" width="auto" height="auto"></img>
          <br></br>
          <br></br>
          <p style="font-size:22.5px; text-align: center; color: black;">ระบบจัดการระบบอินเตอร์เน็ตของวิทยาลัยการอาชีพปัว</p>
        </div>

        <form class="login100-form validate-form" method="post">
          <span class="login100-form-title">
						<p style="font-size:22.5px;">ระบบจัดการอินเตอร์เน็ต</p>
						<p style="font-size:22.5px;"></p>
					</span>

          <div class="wrap-input100 validate-input" data-validate="กรุณาใส่ชื่อผู้ใช้งาน">
            <input class="input100" type="text" id="Admin_Username" name="Admin_Username" placeholder="ชื่อผู้ใช้">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
							<i class="fa fa-user-circle" aria-hidden="true"></i>
						</span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="กรุณาใส่ชื่อผู้ใช้งาน">
            <input class="input100" type="password" id="Admin_Password" name="Admin_Password" placeholder="รหัสผ่าน">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
							<i class="fa fa-user-circle" aria-hidden="true"></i>
						</span>
          </div>
          <div class="container-login100-form-btn">
            <button type="button" class="login100-form-btn" onclick="SendAjaxAction()">ลงชื่อเข้าใช้</button>
          </div>

          <div class="text-center p-t-12">
            <span class="txt1">
								<hr>
						จัดทำโดย<br>
            นายเมธี  ตนะทิพย์<br>
						</hr>
						<hr>
						(เทคโนโลยีสารสนเทศ)
					</hr>
						</span>
          </div>

      </div>
    </div>
  </div>
</body>
<script type="text/javascript">

document.querySelector('#SearchBoxID').addEventListener('keyup', filterTable, false);
  function filterTable(event) {
    var filter = event.target.value.toUpperCase();
    var rows = document.querySelector("#myTable tbody").rows;

    for (var i = 0; i < rows.length; i++) {
        var firstCol = rows[i].cells[0].textContent.toUpperCase();
        var secondCol = rows[i].cells[1].textContent.toUpperCase();
        var thirdcol = rows[i].cells[2].textContent.toUpperCase();
        if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1 || thirdcol.indexOf(filter) > -1) {
            rows[i].style.display = "";
        } else {
            rows[i].style.display = "none";
        }
    }
}
</script>
</html>
