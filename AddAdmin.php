<?php
session_start();


if ($isLoggedin == true) {
} elseif ($debugingcode === false) {
    header("location:./"); ?>

<html lang="en">
<head>
  <meta http-equiv=Content-Type content="text/html; charset=utf-8">
  <title>บันทึกข้อมูล</title>
  <link rel="stylesheet" type="text/css" href="./css/util.css">
<link rel="stylesheet" type="text/css" href="./css/main.css">
</head>
  <script type="text/javascript" src="./js/sweetalert.min.js"></script>


<body class="BodyCus">
<script>
function SavedTheRecord() {
swal({
  title: 'การลงทะเบียนสำเร็จ',
  text: 'สามารถเข้าใช้งานได้ทันที',
  confirmButtonText: 'เข้าใจแล้ว',
  showConfirmButton: true,
  allowOutsideClick: false,
  allowEscapeKey: false,
  type: 'success',
}).then((result) => {
if (result.value) {
    // ทำหลังจากเวลาหมด
    window.location = "./Admin/index.php";
  }
})
};

function FailedToSaveTheRecord() {
swal({
  title: 'ผิดพลาด',
  text: 'การลงทะเบียนผิดพลาดกรุณาลองใหม่',
  confirmButtonText: 'เข้าใจแล้ว',
  showConfirmButton: true,
  allowOutsideClick: false,
  allowEscapeKey: false,
  type: 'error',
}).then((result) => {
if (result.value) {
    // ทำหลังจากเวลาหมด
    window.location = "./AddAdminForm.php";
  }
})
};

function RecaptchaNeeded() {
swal({
  title: 'ผิดพลาด',
  text: 'กรุณายืนยันความเป็นมนุษย์ก่อน',
  timer: 3000,
  showConfirmButton: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
  type: 'error',
}).then((result) => {
if (result.value) {
    // ทำหลังจากเวลาหมด
    window.location = "./AddAdminForm.php";
  }
})
};

function FillAllField() {
swal({
  title: 'ผิดพลาด',
  text: 'กรุณากรอกข้อมูลให้ครบถ้วน',
  timer: 3000,
  showConfirmButton: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
  type: 'error',
}).then((result) => {
if (result.value) {
    // ทำหลังจากเวลาหมด
    window.location = "./AddAdminForm.php";
  }
})
};

</script>

<?php


if ($_SESSION['user_id']) {
    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        $secret = '6Lf-jokUAAAAAKkCEsAsdYe0_Bbjg10_fXS2848L';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if ($responseData->success) {
            require './ConnectToService.php';


            if (isset($_POST['AdminUsername']) && isset($_POST['AdminName']) && isset($_POST['AdminEmail'])) {
                $AdminUserName      = mysqli_real_escape_string($conn, $_POST['AdminUsername']);
                $AdminName          = mysqli_real_escape_string($conn, $_POST['AdminName']);
                $AdminEmail         = mysqli_real_escape_string($conn, $_POST['AdminEmail']);

                $password           = mysqli_real_escape_string($conn, $_POST['AdminPassword']);

                $EncodedPassword    = password_hash($password, PASSWORD_DEFAULT);

                $SqlInsertAdminInfo = "INSERT INTO
  `Administrator_info` (
    `AdminUsername`,
    `AdminPassword`,
    `AdminName`,
    `Email`
  )
VALUES
  ('$AdminUserName', '$EncodedPassword', '$AdminName', '$AdminEmail')";

                if ($conn->query($SqlInsertAdminInfo) === true) {
                    echo '<script type="text/javascript">',
            'SavedTheRecord();',
            '</script>';
                } else {
                    echo '<script type="text/javascript">',
            'FailedToSaveTheRecord();',
            '</script>';
                }
            } else {
                echo '<script type="text/javascript">',
      'FillAllField();',
      '</script>';
            }
        } else {
            echo '<script type="text/javascript">',
  'RecaptchaNeeded();',
  '</script>';
        }
    }
} else {
    $isLoggedin = false;
}
}
?>
